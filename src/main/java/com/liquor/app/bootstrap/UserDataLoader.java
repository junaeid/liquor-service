package com.liquor.app.bootstrap;

import com.liquor.app.model.User;
import com.liquor.app.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@Order(1)
public class UserDataLoader implements CommandLineRunner {

    private final UserRepository userRepository;

    public UserDataLoader(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        User admin1 = User.builder()
                .username("admin")
                .password("admin")
                .birthday(LocalDate.now())
                .build();
        userRepository.save(admin1);

        User user1 = User.builder()
                .username("user1")
                .password("user")
                .birthday(LocalDate.now())
                .build();
        userRepository.save(user1);

        User user2 = User.builder()
                .username("user2")
                .password("user")
                .birthday(LocalDate.now())
                .build();
        userRepository.save(user2);

        User user3 = User.builder()
                .username("user3")
                .password("user")
                .birthday(LocalDate.now())
                .build();
        userRepository.save(user3);


    }
}
