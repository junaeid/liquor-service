package com.liquor.app.bootstrap;

import com.liquor.app.model.Bottle;
import com.liquor.app.repository.BottleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class BottleDataLoader implements CommandLineRunner {

    private final BottleRepository bottleRepository;

    public BottleDataLoader(BottleRepository bottleRepository) {
        this.bottleRepository = bottleRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Bottle bottle = Bottle.builder()
                .name("Ariel Non-alcoholic Wine")
                .pic("https://m.media-amazon.com/images/I/318E5k6WdxL._AC_.jpg")
                .inStock(12)
                .supplier("Ariel")
                .price(32)
                .volume(500)
                .volumePercent(0)
                .build();

        bottleRepository.save(bottle);
    }
}
