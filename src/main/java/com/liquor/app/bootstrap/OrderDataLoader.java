package com.liquor.app.bootstrap;

import com.liquor.app.model.Order;
import com.liquor.app.repository.OrderRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class OrderDataLoader implements CommandLineRunner {

    private final OrderRepository orderRepository;

    public OrderDataLoader(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Order order1 = Order.builder()
                .price(23)
                .build();

        orderRepository.save(order1);

        Order order2 = Order.builder()
                .price(24)
                .build();
        orderRepository.save(order2);


    }
}
