package com.liquor.app.bootstrap;

import com.liquor.app.model.Address;
import com.liquor.app.repository.AddressRepository;
import com.liquor.app.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class AddressDataLoader implements CommandLineRunner {
    private final AddressRepository addressRepository;
    private final UserRepository userRepository;

    public AddressDataLoader(AddressRepository addressRepository, UserRepository userRepository) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Address address1 = Address.builder()
                .street("21 jump street")
                .postalCode("12345")
                .number("234234")
                .user(userRepository.getById(1L))
                .build();
        addressRepository.save(address1);
    }
}
