package com.liquor.app.bootstrap;

import com.liquor.app.model.Crate;
import com.liquor.app.repository.CrateRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class CrateDataLoader implements CommandLineRunner {

    private final CrateRepository crateRepository;

    public CrateDataLoader(CrateRepository crateRepository) {
        this.crateRepository = crateRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Crate crate1 = Crate.builder()
                .noOfBottles(6)
                .inStock(6)
                .price(13)
                .name("meori Foldable")
                .pic("https://m.media-amazon.com/images/I/61+Tr1l3B+L._AC_SL1200_.jpg")
                .build();

        crateRepository.save(crate1);

    }
}
