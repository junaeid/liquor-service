package com.liquor.app.repository;

import com.liquor.app.model.Bottle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BottleRepository extends JpaRepository<Bottle, Long> {
    Bottle getBottleById(Long id);
}
