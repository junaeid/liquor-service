package com.liquor.app.repository;

import com.liquor.app.model.Order;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    @EntityGraph(value = "Order.orderItems")
    @Override
    List<Order> findAll();
}
