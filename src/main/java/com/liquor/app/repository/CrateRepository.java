package com.liquor.app.repository;

import com.liquor.app.model.Crate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrateRepository extends JpaRepository<Crate, Long> {
    Crate getCrateById(Long id);
}
