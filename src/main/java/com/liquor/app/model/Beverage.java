package com.liquor.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Beverage {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @NotNull(message = "name must not be null")
    @NotEmpty(message = "name must not be empty")
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "name must contain only strings")
    private String name;
    private String pic;

    @Min(value = 1, message = "price is a positive number")
    private int price;
    @Min(value = 0, message = "stock is a positive number")
    private int inStock;

}
