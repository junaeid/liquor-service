package com.liquor.app.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@SuperBuilder

public class DateRangeFieldValidator {
    private LocalDate startDate;
    private LocalDate endDate;
}
