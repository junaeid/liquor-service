package com.liquor.app.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
/*
@NamedEntityGraph(
        name = "User.orders"
)*/
public class User extends BaseEntity {

    //unique
    @NotNull(message = "username must not be null")
    @NotEmpty(message = "username must not be empty")
    private String username;

    @NotNull(message = "password must not be null")
    @NotEmpty(message = "password must not be empty")
    private String password;

    //LocalDate(>1.1.1900, <today)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    //@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Order> orders;


    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Address> addresses;

}

