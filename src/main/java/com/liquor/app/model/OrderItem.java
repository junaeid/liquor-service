package com.liquor.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class OrderItem extends BaseEntity {
    private String position; //only digits
    private double price;

    private int quantity;

    @ManyToOne
    @JoinColumn(name = "beverage_id")
    private Beverage beverage;


    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public OrderItem(Beverage beverage, int quantity) {
        this.beverage = beverage;
        this.quantity = quantity;
    }
}
