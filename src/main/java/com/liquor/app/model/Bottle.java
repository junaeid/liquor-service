package com.liquor.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.type.TrueFalseType;

import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
public class Bottle extends Beverage {
    @Min(value = 1, message = "volume must be greater than zero")
    private double volume;
    @Min(value = 0, message = "percentage must be positive number")
    private double volumePercent;
    @NotNull(message = "supplier name must not be null")
    @NotEmpty(message = "supplier name must not be empty")
    private String supplier;

    private boolean isAlcoholic;

    public void setVolumePercent(double volumePercent) {
        this.volumePercent = volumePercent;
        updateIsAlcoholicStatus(volumePercent);
    }

    private void updateIsAlcoholicStatus(double volumePercent) {
        if (volumePercent > 0) setAlcoholic(true);
    }

}

