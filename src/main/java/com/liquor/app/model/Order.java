package com.liquor.app.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@NamedEntityGraph(
        name = "Order.orderItems",
        attributeNodes = {
                @NamedAttributeNode("orderItems"),
                @NamedAttributeNode("user")
        }
)
@Table(name = "orders")
public class Order extends BaseEntity {

    @Column(name = "price")
    private double price;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private List<OrderItem> orderItems;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
