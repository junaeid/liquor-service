package com.liquor.app.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@SuperBuilder
@Entity
public class Address extends BaseEntity {

    @NotNull(message = "street must not be null")
    @NotEmpty(message = "street must not be empty")
    private String street;

    @NotNull(message = "Number must not be null")
    @NotEmpty(message = "Number must not be empty")
    private String number;

    @NotNull(message = "Postal Code must not be null")
    @NotEmpty(message = "Postal Code must not be empty")
    //onlt contains 5 digits
    @Size(min = 5, max = 5)
    //@Pattern(regexp = "^[0-9]")
    private String postalCode;

    //@LazyCollection(LazyCollectionOption.FALSE)
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
