package com.liquor.app.service;

import com.liquor.app.model.Order;

import java.util.List;

public interface OrderService {
    List<Order> getAllOrders();

    Order getOrderById(Long id);
}
