package com.liquor.app.service.impl;

import com.liquor.app.model.Bottle;
import com.liquor.app.repository.BottleRepository;
import com.liquor.app.service.BottleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BottleServiceImpl implements BottleService {

    private final BottleRepository bottleRepository;

    public BottleServiceImpl(BottleRepository bottleRepository) {
        this.bottleRepository = bottleRepository;
    }

    @Override
    public List<Bottle> getAllBottles() {
        return bottleRepository.findAll();
    }

    @Override
    public void addNewBottle(Bottle bottle) {
        bottleRepository.save(bottle);
    }

    @Override
    public Bottle getBottleById(Long id) {
       return bottleRepository.findById(id).orElse(null);
    }
}
