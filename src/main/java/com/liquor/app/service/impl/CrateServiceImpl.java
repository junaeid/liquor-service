package com.liquor.app.service.impl;

import com.liquor.app.model.Crate;
import com.liquor.app.repository.CrateRepository;
import com.liquor.app.service.CrateService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrateServiceImpl implements CrateService {

    private final CrateRepository crateRepository;

    public CrateServiceImpl(CrateRepository crateRepository) {
        this.crateRepository = crateRepository;
    }

    @Override
    public List<Crate> getAllCrates() {
        return crateRepository.findAll();
    }

    @Override
    public void addNewCrate(Crate crate) {
        crateRepository.save(crate);
    }

    @Override
    public Crate getCrateById(Long id) {
        return crateRepository.findById(id).orElse(null);
    }
}
