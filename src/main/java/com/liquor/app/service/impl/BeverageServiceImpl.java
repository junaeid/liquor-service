package com.liquor.app.service.impl;

import com.liquor.app.model.Beverage;
import com.liquor.app.repository.BeverageRepository;
import com.liquor.app.service.BeverageService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeverageServiceImpl implements BeverageService {
    private final BeverageRepository beverageRepository;

    public BeverageServiceImpl(BeverageRepository beverageRepository) {
        this.beverageRepository = beverageRepository;
    }

    @Override
    public List<Beverage> getAllBeverages() {
        return beverageRepository.findAll();
    }

    @Override
    public Beverage getBeverageById(Long id) {
        return beverageRepository.getById(id);
    }

}
