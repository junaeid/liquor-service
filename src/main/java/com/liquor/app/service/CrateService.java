package com.liquor.app.service;

import com.liquor.app.model.Crate;

import java.util.List;

public interface CrateService {
    List<Crate> getAllCrates();

    void addNewCrate(Crate crate);

    Crate getCrateById(Long id);
}
