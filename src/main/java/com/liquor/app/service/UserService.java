package com.liquor.app.service;

import com.liquor.app.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUser();

    void addNewUser(User user);
}
