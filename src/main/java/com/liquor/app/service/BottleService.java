package com.liquor.app.service;

import com.liquor.app.model.Bottle;

import java.util.List;

public interface BottleService {
    List<Bottle> getAllBottles();

    void addNewBottle(Bottle bottle);

    Bottle getBottleById(Long id);
}
