package com.liquor.app.service;

import com.liquor.app.model.Beverage;

import java.util.List;

public interface BeverageService {
    List<Beverage> getAllBeverages();

    Beverage getBeverageById(Long id);
}
