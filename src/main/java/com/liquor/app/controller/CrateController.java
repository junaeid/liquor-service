package com.liquor.app.controller;

import com.liquor.app.model.Crate;
import com.liquor.app.service.CrateService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("crate")
public class CrateController {

    private final CrateService crateService;

    public CrateController(CrateService crateService) {
        this.crateService = crateService;
    }

    @GetMapping
    public String index(
            Model model
    ) {
        model.addAttribute("crates", crateService.getAllCrates());
        return "crate/index";
    }

    @GetMapping("{id}")
    public String getCrateById(
            @PathVariable("id") Long id,
            Model model
    ) {
        model.addAttribute("crate", crateService.getCrateById(id));
        return "crate/details";
    }

    @GetMapping("new")
    public String addNewCrate(
            @ModelAttribute("crate") Crate crate
    ) {
        return "crate/new-crate";
    }

    @PostMapping("new")
    public String addNewCrate(
            @ModelAttribute("crate") @Valid Crate crate,
            Errors errors
    ) {
        if (errors != null && errors.getErrorCount() > 0) {
            return "crate/new-crate";
        }
        crateService.addNewCrate(crate);
        return "redirect:/crate";
    }
}
