package com.liquor.app.controller;

import com.liquor.app.model.OrderItem;
import com.liquor.app.service.BottleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("cart")
public class CartController {
    private final BottleService bottleService;

    public CartController(BottleService bottleService) {
        this.bottleService = bottleService;
    }

    @GetMapping
    public String index() {
        return "cart/index";
    }

    @GetMapping("{id}")
    public String buy(@PathVariable("id") Long id, HttpSession session) {

        if (session.getAttribute("cart") == null) {
            List<OrderItem> cart = new ArrayList<>();

            cart.add(new OrderItem(bottleService.getBottleById(id), 1));

            session.setAttribute("cart", cart);
            session.setAttribute("total", getTotalPriceOfTheProduct(cart));

        } else {
            @SuppressWarnings("unchecked")
            List<OrderItem> cart = (List<OrderItem>) session.getAttribute("cart");

            int index = this.exists(id, cart);
            if (index == -1) {
                cart.add(new OrderItem(bottleService.getBottleById(id), 1));

            } else {
                int quantity = cart.get(index).getQuantity() + 1;
                cart.get(index).setQuantity(quantity);
            }

            session.setAttribute("cart", cart);
            session.setAttribute("total", getTotalPriceOfTheProduct(cart));

        }

        return "redirect:/cart";
    }


    @GetMapping("remove/{id}")
    public String remove(@PathVariable("id") Long id, HttpSession session) {
        @SuppressWarnings("unchecked")
        List<OrderItem> cart = (List<OrderItem>) session.getAttribute("cart");

        cart.remove(this.exists(id, cart));
        session.setAttribute("cart", cart);
        session.setAttribute("total", getTotalPriceOfTheProduct(cart));

        return "redirect:/cart";
    }

    private int exists(Long id, List<OrderItem> cart) {
        for (int i = 0; i < cart.size(); i++) {

            if (cart.get(i).getBeverage().getId().equals(id)) {
                //System.out.println(i + " " + cart.get(i).getProduct().getId() + " " + cart.get(i).getProduct().getId().equals(id));
                return i;
            }
        }
        return -1;
    }

    private int getTotalPriceOfTheProduct(List<OrderItem> cart) {
        return cart.stream()
                .mapToInt(item -> (int) item.getBeverage().getPrice() * item.getQuantity())
                .sum();
    }


}
