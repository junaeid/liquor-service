package com.liquor.app.controller;

import com.liquor.app.model.Beverage;
import com.liquor.app.model.Bottle;
import com.liquor.app.model.Crate;
import com.liquor.app.service.BeverageService;
import com.liquor.app.service.BottleService;
import com.liquor.app.service.CrateService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class BeverageController {

    private final BeverageService beverageService;
    private final BottleService bottleService;
    private final CrateService crateService;

    public BeverageController(BeverageService beverageService, BottleService bottleService, CrateService crateService) {
        this.beverageService = beverageService;
        this.bottleService = bottleService;
        this.crateService = crateService;
    }

    @GetMapping
    public String index(
            Model model
    ) {
        model.addAttribute("beverages", beverageService.getAllBeverages());
        return "beverage/index";
    }

    @GetMapping("{id}")
    public String getBeverageById(
            @PathVariable("id") Long id,
            Model model
    ) {
        if (bottleService.getBottleById(id) != null) {
            model.addAttribute("bottle", bottleService.getBottleById(id));
            return "/bottle/details";
        } else if (crateService.getCrateById(id) != null) {
            model.addAttribute("crate", crateService.getCrateById(id));
            return "/crate/details";
        } else {
            return "redirect:/";
        }
    }
}
