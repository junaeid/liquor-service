package com.liquor.app.controller;

import com.liquor.app.model.Address;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@Controller
@RequestMapping("address")
public class AddressController {

    @GetMapping("{id}")
    public String addNew(
            @ModelAttribute("address") Address address,
            @PathParam("id") Long id
    ) {
        return "address/new";
    }

    @PostMapping("{id}")
    public String addNew(
            @ModelAttribute("address") @Valid Address address,
            @PathParam("id") Long id,
            Errors errors
    ) {
        if (errors != null && errors.getErrorCount() > 0) {
            return "address/{" + id + "}";
        }
        return "redirect:/user";
    }
}
