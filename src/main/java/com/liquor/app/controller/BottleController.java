package com.liquor.app.controller;

import com.liquor.app.model.Bottle;
import com.liquor.app.service.BottleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("bottle")
public class BottleController {

    private final BottleService bottleService;

    public BottleController(BottleService bottleService) {
        this.bottleService = bottleService;
    }

    @GetMapping
    public String index(
            Model model
    ) {
        model.addAttribute("bottles", bottleService.getAllBottles());
        return "bottle/index";
    }

    @GetMapping("{id}")
    public String getBottleById(
            Model model,
            @PathVariable("id") Long id
    ) {
        model.addAttribute("bottle", bottleService.getBottleById(id));
        return "bottle/details";
    }

    @GetMapping("new")
    public String addNewBottle(
            @ModelAttribute("bottle") Bottle bottle
    ) {
        return "bottle/new-bottle";
    }

    @PostMapping("new")
    public String addNewBottle(
            @ModelAttribute("bottle") @Valid Bottle bottle,
            Errors errors
    ) {
        if (errors != null && errors.getErrorCount() > 0) {
            return "/bottle/new-bottle";
        }
        bottleService.addNewBottle(bottle);
        return "redirect:/bottle";
    }
}
