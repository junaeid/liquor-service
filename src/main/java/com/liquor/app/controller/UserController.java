package com.liquor.app.controller;

import com.liquor.app.model.User;
import com.liquor.app.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String index(
            Model model
    ) {
        model.addAttribute("users", userService.getAllUser());
        return "user/index";
    }

    @GetMapping("new")
    public String addNewUser(
            @ModelAttribute("user") User user
    ) {
        return "user/new";
    }

    @PostMapping("new")
    public String addNewUser(
            @ModelAttribute("user") @Valid User user,
            Errors errors
    ) {
        if (errors != null && errors.getErrorCount() > 0) {
            return "user/new";
        }
        userService.addNewUser(user);
        return "redirect:/user";
    }
}
