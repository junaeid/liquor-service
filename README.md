# Liqure Service

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
         <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#deployment">Deployment</a></li>
      </ul>
    </li>
    <li>
        <a href="#bugs">Bugs</a>
    </li>
    <li>
        <a href="#future-implementation">Future Implementation</a>
    </li>
    <li>
        <a href="#conclusion">Conclusion</a>
    </li>
  </ol>
</details>

### About The Project
This is a full stack web application of a Beverage Service, which is deployed with functionalities with respect to security, user management and
tests. The following helps to understand what all and how the project was implemented.

### Built With
- Springboot(2.6.4)
- hibernate
- Thymeleaf
- Lombok
- NameBased Entity
- Database used InMemory Database(h2)

### Prerequisites
- IntelliJ IDEA(recommended)
- Java11/OpenJdk11
- Gradle




### Bugs
...

### Future Implementation

- Data transfer object(DTO) class
- Image Upload
- OrderController and template page
- OrderItemController and template page
- Generate Pdf for the Bills
- TestCase based on the requirement

### Conclusion
These are the features and implementations that are included,tried adding but failed and yet to be added in this Beverage Service application and  that is how it works. 



